# Distributeur de boisson

## Contexte

Afin d'optimiser les coûts de fonctionnement et de maintenance d'un distributeur de boisson on souhaite mettre en place une communication avec serveur qui permettra de connaître à distance l'état de la machine

## Installation

La librairie GTK est nécessaire pour faire fonctionner l'interface graphique.
Pour les distributions basées sur debian:  
`sudo apt install libgtk-3-0`  
Pour Fedora:  
`dnf install gtk3`

Par la suite cloner le projet:  
```bash
git clone https://gitlab.com/NBrette/coffee.git
cd coffee

#Compilation du client
make client
#Compilation du serveur
make server
```

## Utilisation

Avant de lancer le programme il est nécessaire de le paramétrer. Le dossier common contient un fichier de configuration pour le client

Paramètres du serveur:
```ini
[server_network]
ip=127.0.0.1
port=7799

[machine]
qte_cafe=1000
qte_the=1000
qte_lait=1000
qte_sucre=1000
qte_soupe=1000
```

Une fois paramétré, le programme peut être lancé.
Pour démarrer le serveur:  
`make runServer`  
Pour démarrer le client:  
`make runClient`  

Le fichier `src/server/log.txt` contient les logs écrits par la serveur