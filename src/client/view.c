#include <gtk/gtk.h>
#include <time.h>
#include <string.h>
#include "controller.h"
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <stdbool.h>
#include "variables.h"


GtkBuilder *builder = NULL;
MachineSettings machine_settings;
int sockfd;
int status = VEILLE_STATUS;

/**
 * @brief Close the windows and disconnect from the server
 **/
void on_window_main_destroy()
{
    gtk_main_quit();
}

/**
 * @brief Change the status of the machine
 * @param status
 **/
void change_status(int status){
    char label_content[128];
    GtkLabel *label_status = GTK_LABEL(gtk_builder_get_object(builder, "status"));
    switch (status)
    {
    case VEILLE_STATUS:
        sprintf(label_content, "%s", "Veille");
        gtk_label_set_text(label_status, label_content);
        break;
    case PRECHAUFFAGE_STATUS:
        sprintf(label_content, "%s", "Préchauffage");
        gtk_label_set_text(label_status, label_content);
        break;
    case WORKING_STATUS:
        sprintf(label_content, "%s", "En marche");
        gtk_label_set_text(label_status, label_content);
        break;
    case COFFEE_STATUS:
        sprintf(label_content, "%s", "Café en cours");
        gtk_label_set_text(label_status, label_content);
        break;
    case TEA_STATUS:
        sprintf(label_content, "%s", "Thé en cours");
        gtk_label_set_text(label_status, label_content);
        break;
    case SOUP_STATUS:
        sprintf(label_content, "%s", "Soupe en cours");
        gtk_label_set_text(label_status, label_content);
        break;
    default:
        break;
    }
}

/**
 * @brief Event for the coffee button
 **/
void on_button_cafe()
{
    
    if (status == VEILLE_STATUS){
        demarrage(sockfd);
        prechauffage(sockfd);
        sleep(2);
        status = WORKING_STATUS;
        change_status(status);
    }
    cafe(sockfd, &machine_settings);

}



/**
 * @brief Event for the tea button
 **/
void on_button_the()
{   
    if (status == VEILLE_STATUS){
        demarrage(sockfd);
        prechauffage(sockfd);
        status = WORKING_STATUS;
        change_status(status);
    }
    the(sockfd, &machine_settings);
    
}
    
   

/**
 * @brief Event for the soup button
 **/
void on_button_soupe()
{
    if (status == VEILLE_STATUS){
        demarrage(sockfd);
        prechauffage(sockfd);
        status = WORKING_STATUS;
        change_status(status);
    }
    soupe(sockfd, &machine_settings);
}


/**
 * @brief Leave the game and disconnect
 **/
void on_cancel()
{
    GtkWidget *message_dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_MODAL,
                                                       GTK_MESSAGE_WARNING,
                                                       GTK_BUTTONS_OK_CANCEL,
                                                       "This action will cause the universe to stop existing.");
    unsigned int pressed = gtk_dialog_run(GTK_DIALOG(message_dialog));
    if (pressed == GTK_RESPONSE_OK)
    {
        printf("\nDisconnect.. \n");
        gtk_widget_destroy(message_dialog);
        gtk_main_quit();
    }
    else
    {
        gtk_widget_destroy(message_dialog);
    }
}

/**
 * @brief Function to initialize the client interface with reading thread
 * @param argc
 * @param argv
 * @param sockfdd
 * @param thread
 **/
void initialize_window(int argc, char **argv, int sockfdd, MachineSettings settings)
{
    GtkWidget *win;
    sockfd = sockfdd;
    machine_settings = settings;
    gtk_init(&argc, &argv);
    builder = gtk_builder_new_from_file("gui/Interface.glade");
    win = GTK_WIDGET(gtk_builder_get_object(builder, "app_win"));
    g_object_set(gtk_settings_get_default(), "gtk-application-prefer-dark-theme", TRUE, NULL);
    gtk_builder_connect_signals(builder, NULL);
    gtk_widget_show(win);


    gtk_main();
}
