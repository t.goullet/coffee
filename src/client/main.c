#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include "server_connection.h"
#include "view.h"



int main(int argc, char** argv)
{
	pthread_t thread;
	NetworkSettings settings;
	MachineSettings machine_settings;
	int sockfd;
	get_machine_settings(&machine_settings, "../common/config_machine.ini");
	get_network_settings(&settings, "../common/config_machine.ini");
	sockfd = open_connection(settings);
	initialize_window(argc, argv, sockfd, machine_settings);
	
	
	return (0);
}