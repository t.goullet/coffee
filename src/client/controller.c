#include "variables.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include "../common/config_reader.h"


/**
 * @brief Start the machine
 * @param sockfd
 **/
void demarrage(int sockfd){ 
    send_message(sockfd, "Demarrage de la machine");
    sleep(2);
}

/**
 * @brief Warm the water
 * @param sockfd
 **/
void prechauffage(int sockfd){
    send_message(sockfd, "Préchauffage");
    sleep(2);
}

/**
 * @brief Make the coffee
 * @param sockfd
 * @param machineSettings
 **/
void cafe(int sockfd, MachineSettings *machineSettings){
    send_message(sockfd, "Café demandé");
    sleep(1);
    send_message(sockfd, "Descente du gobelet");
    sleep(1);
    send_message(sockfd, "Dose de café");
    sleep(1);
    send_message(sockfd, "Ajout de l'eau");
    sleep(1);
    send_message(sockfd, "Ajout touillette");
    sleep(1);
    machineSettings->qte_cafe--;
    machineSettings->qte_sucre--;
    printf("Café : %d, Sucre : %d\n", machineSettings->qte_cafe, machineSettings->qte_sucre);
}

/**
 * @brief Make the tea
 * @param sockfd
 * @param machineSettings
 **/
void the(int sockfd, MachineSettings *machineSettings){
    send_message(sockfd, "Thé demandé");
    sleep(1);
    send_message(sockfd, "Descente du gobelet");
    sleep(1);
    send_message(sockfd, "Dose de thé");
    sleep(1);
    send_message(sockfd, "Ajout de l'eau");
    sleep(1);
    machineSettings->qte_the--;
    machineSettings->qte_sucre--;
    printf("Thé : %d, Sucre : %d\n", machineSettings->qte_the, machineSettings->qte_sucre);
}

/**
 * @brief Make the soup
 * @param sockfd
 * @param machineSettings
 **/
void soupe(int sockfd, MachineSettings *machineSettings){
    send_message(sockfd, "Soupe demandé");
    sleep(1);
    send_message(sockfd, "Descente du gobelet");
    sleep(1);
    send_message(sockfd, "Dose de soupe");
    sleep(1);
    send_message(sockfd, "Ajout de l'eau");
    sleep(1);
    machineSettings->qte_soupe--;
    
}

/**
 * @brief Send a mesage to the server
 * @param sockfd
 * @param message
 **/
void send_message(int sockfd, char *message){
    write(sockfd, message, strlen(message));
}



