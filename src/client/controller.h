#include <stdbool.h>
#include "../common/config_reader.h"

void demarrage(int sockfd);
void prechauffage(int sockfd);
void cafe(int sockfd, MachineSettings *machineSettings);
void soupe(int sockfd, MachineSettings *machineSettings);