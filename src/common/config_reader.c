#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "ini.h"
#include "config_reader.h"

/**
 * @brief Read the config file for the network and put the values in the structure
 * @param settings
 * @param filename
 **/
 
void get_network_settings(NetworkSettings *settings, char *filename){
    ini_t *config_server = ini_load(filename);
    char *IP = ini_get(config_server, "server_network", "ip");
    char *port = ini_get(config_server, "server_network", "port");
    settings->port = atoi(port);
    settings->ip = IP;
}

/**
 * @brief Read the config file for the machine quantity and put the values in the structure
 * @param settings
 * @param filename
 **/
void get_machine_settings(MachineSettings *settings, char *filename){
    ini_t *config_server = ini_load(filename);
    char *qte_cafe = ini_get(config_server, "machine", "qte_cafe");
    char *qte_the = ini_get(config_server, "machine", "qte_the");
    char *qte_lait = ini_get(config_server, "machine", "qte_lait");
    char *qte_sucre = ini_get(config_server, "machine", "qte_sucre");
    char *qte_soupe = ini_get(config_server, "machine", "qte_soupe");
    settings->qte_cafe = atoi(qte_cafe);
    settings->qte_the = atoi(qte_the);
    settings->qte_lait = atoi(qte_lait);
    settings->qte_sucre = atoi(qte_sucre);
    settings->qte_soupe = atoi(qte_soupe);
}