typedef struct NetworkSettings{
    char *ip;
    int *port;
} NetworkSettings;

typedef struct MachineSettings{
    int qte_cafe;
    int qte_the;
    int qte_lait;
    int qte_sucre;
    int qte_soupe;
} MachineSettings;

void get_network_settings(NetworkSettings *settings, char *filename);
void get_machine_settings(MachineSettings *settings, char *filename);

