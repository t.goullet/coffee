#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void create_log_file()
{
    int logger = fopen("log.txt", "w");
    if (logger == NULL)
    {
        printf("Erreur lors de la création du fichier log\n");
        exit(1);
    }
}
char* get_time()
{
    time_t rawtime;
    struct tm * timeinfo;
    char *time_str;

    time ( &rawtime );
    timeinfo = localtime ( &rawtime );
    time_str = asctime (timeinfo);
    time_str[strcspn(time_str, "\n")] = 0;
    return time_str;
}

void log_data(char *data)
{
    char str[256];
    strcpy(str, get_time());
    strcat(str, " : ");
    strcat(str, data);
    printf("%s ", str);

    FILE *logger = fopen("log.txt", "a");
    if (logger == NULL)
    {
        printf("Erreur lors de l'ouverture du fichier log\n");
        exit(1);
    }
    fprintf(logger, "%s\n", str);
    fclose(logger);
}



