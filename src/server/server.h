#include <sys/socket.h>

// Structure of connection socket
typedef struct {
    int sockfd;
    struct sockaddr address;
    int addr_len;
    int index;
} connection_t;

#define BUFFERSIZE 2048

int create_server_socket();
void process_input(void *ptr);