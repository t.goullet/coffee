#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "server.h"
#include <sys/socket.h>
#include <unistd.h>
#include <arpa/inet.h>
#include "logger.h"



/**
 * @brief init socket when server started 
 * @param setting
 **/
int create_server_socket() {

    
    int sockfd = -1;
    struct sockaddr_in address;
    int port = 7799;

    /* create socket */
    sockfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (sockfd <= 0) {
        fprintf(stderr, "%s: error: cannot create socket\n");
        return -3;
    }


    /* bind socket to port */
    address.sin_family = AF_INET;
    //bind to all ip : 
    //address.sin_addr.s_addr = INADDR_ANY;
    //ou 0.0.0.0 
    //Sinon  127.0.0.1
    address.sin_addr.s_addr = inet_addr("127.0.0.1");
    address.sin_port = htons(port);

    /* prevent the 60 secs timeout */
    int reuse = 1;
    setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, (const char*) &reuse, sizeof (reuse));
    /* bind */
    if (bind(sockfd, (struct sockaddr *) &address, sizeof (struct sockaddr_in)) < 0) {
        fprintf(stderr, "error: cannot bind socket to port %d\n", port);
        return -4;
    }

    return sockfd;
}

/**
 * @brief Handle the messages when they are received
 **/
void process_input(void *ptr){
    char inputClient[BUFFERSIZE];

    int len;
    connection_t *connection;

    connection = (connection_t *) ptr;

    while ((len = read(connection->sockfd, inputClient, BUFFERSIZE)) > 0) {
        printf("Input : %s \n", inputClient);
        log_data(inputClient);
        memset(inputClient, '\0', sizeof(inputClient));
        }
    close(connection->sockfd);
    free(connection);
    printf("Connection closed\n");
}