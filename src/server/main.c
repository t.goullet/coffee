#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include "server.h"
#include "logger.h"



int main(int argc, char **argv)
{
	int sockfd = -1;
    int index = 1;
    connection_t *connection;
    pthread_t thread;

    sockfd = create_server_socket();
    create_log_file();

        if (listen(sockfd, 150) < 0)
    {
        fprintf(stderr, "%s: error: cannot listen on port\n", argv[0]);
        return -5;
    }
    while (true)
    {
        /* accept incoming connections */
        printf("Server ready and listening\n");
        connection = (connection_t *)malloc(sizeof(connection_t));
        connection->sockfd = accept(sockfd, &connection->address, &connection->addr_len);
        connection->index = index++;
        
        if (connection->sockfd <= 0)
        {
            free(connection);
        }
        else
        {
            /* start a new thread but do not wait for it */
            pthread_create(&thread, 0, process_input, (void *)connection);
            pthread_detach(thread);
        }
    }

	return (0);
}