CD=cd
DIRSERVER=./src/server/
DIRCLIENT=./src/client/
DIRCOMMON=./src/common/
GTKthread=-pthread `pkg-config --cflags --libs gtk+-3.0` -rdynamic
thread=-pthread

server:
	${CD} $(DIRSERVER) && gcc -c *.c
	gcc ${thread} -o $(DIRSERVER)server $(DIRSERVER)*.o 

client:
	$(CD) $(DIRCLIENT) && gcc -c *.c ${GTKthread}
	${CD} $(DIRCOMMON) && gcc -c *.c
	gcc -o $(DIRCLIENT)client $(DIRCLIENT)*.o $(DIRCOMMON)*.o ${GTKthread}

runServer:
	${CD} $(DIRSERVER) && ./server

runClient:
	${CD} $(DIRCLIENT) && ./client
